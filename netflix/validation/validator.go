package validation

import ( val "gopkg.in/go-playground/validator.v9" )

// create a global validator
var Validator *val.Validate


// function to instanceate a validator
func CreateValidator() {

	// call create validator
	Validator = val.New()

	// create custom validation for netflix plan name
	Validator.RegisterValidation("netflix-plan", ValidateNetflixPlan)
}


// function to validate netflix plan names
func ValidateNetflixPlan(field val.FieldLevel) bool {

	name := field.Field().String()

	return name == "basic" || name == "default" || name == "premium"  
}