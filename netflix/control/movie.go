package control

import (
	"encoding/json"
	"github.com/bragamateus/class-codes/class-codes-netflix/netflix/database"
	"github.com/bragamateus/class-codes/class-codes-netflix/netflix/models"
	"github.com/bragamateus/class-codes/class-codes-netflix/netflix/validation"
	"io/ioutil"
	"log"
	"net/http"
)

func RegisterMovie(w http.ResponseWriter, r *http.Request) {

	// retrieve body from request
	body := r.Body

	// declare use entity
	var movie models.Movie

	// parsing io.ReadCLoser to slice of bytes []byte
	bytes, _ := ioutil.ReadAll(body)

	// parses json into user struct
	err := json.Unmarshal(bytes, &movie)

	// checks if any error occurs in json parsing
	if err != nil {

		log.Printf("[WARN] problem parsing json body, because, %v\n", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// checks if struct is a valid one
	if err := validation.Validator.Struct(movie); err != nil {

		log.Printf("[WARN] invalid user information, because, %v\n", err)
		w.WriteHeader(http.StatusPreconditionFailed)
		return
	}

	err = database.InsertMovie(movie)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Write([]byte(`{"message": "success"}`))
}

func SearchMovieByCategory(w http.ResponseWriter, r *http.Request) {

	q := r.URL.Query()

	value := q["category"][0]
	m , err := database.SearchMovie(value)

	if err != nil {
		log.Printf("[ERROR] category not found: %v", err)
		w.Write([]byte("404"))
		return
	}

	userJson, err := json.Marshal(m)
	if err != nil {
		log.Printf("[ERROR] Invalid category: %v", err)
		w.Write([]byte("400"))
		return
	}

	w.Write([]byte(userJson))

	return
}
