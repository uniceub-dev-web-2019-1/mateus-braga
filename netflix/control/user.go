package control

import (
	"github.com/bragamateus/class-codes/class-codes-netflix/netflix/database"
	"github.com/bragamateus/class-codes/class-codes-netflix/netflix/models"
	"github.com/bragamateus/class-codes/class-codes-netflix/netflix/validation"
	"log"
		 "io/ioutil"
		 "net/http"
		 "encoding/json"
		  )


func RegisterUser(w http.ResponseWriter, r *http.Request) {

	// retrieve body from request
	body := r.Body

	// declare use entity
	var user models.User

	// parsing io.ReadCLoser to slice of bytes []byte
	bytes, _ := ioutil.ReadAll(body)

	// parses json into user struct
	err := json.Unmarshal(bytes, &user)

	// checks if any error occurs in json parsing  
	if err != nil {

		log.Printf("[WARN] problem parsing json body, because, %v\n", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// checks if struct is a valid one
	if err := validation.Validator.Struct(user); err != nil {

		log.Printf("[WARN] invalid user information, because, %v\n", err)
		w.WriteHeader(http.StatusPreconditionFailed)
		return
	}

	err = database.InsertUser(user)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Write([]byte(`{"message": "success"}`))
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {

	// retrieve body from request
	body := r.Body

	// declare user entity
	var updatableUser models.UpdatableUser

	// parsing io.ReadCLoser to slice of bytes []byte
	bytes, _ := ioutil.ReadAll(body)

	// parses json into user struct
	err := json.Unmarshal(bytes, &updatableUser)

	// checks if any error occurs in json parsing  
	if err != nil {

		log.Printf("[WARN] problem parsing json body, because, %v\n", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// TODO: call database function 

	w.Write([]byte(`{"message": "success"}`))


}

func LoginUser(w http.ResponseWriter, r *http.Request) {

	// retrieve body from request
	body := r.Body

	// declare use entity
	var login models.LoginUser

	// parsing io.ReadCLoser to slice of bytes []byte
	bytes, _ := ioutil.ReadAll(body)

	// parses json into user struct
	err := json.Unmarshal(bytes, &login)

	// checks if any error occurs in json parsing  
	if err != nil {

		log.Printf("[WARN] problem parsing json body, because, %v\n", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// checks if struct is a valid one
	if err := validation.Validator.Struct(login); err != nil {

		log.Printf("[WARN] invalid user information, because, %v\n", err)
		w.WriteHeader(http.StatusPreconditionFailed)
		return
	}

	resp, err := database.SearchUser(login)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Write([]byte(resp.Pass))
}
