package models

type User struct {

	Email   string        `json:"email"   validate:"required,email"`
	Pass    string        `json:"pass"    validate:"required,max=12,min=8,alpha"`
	Plan    Plan          `json:"plan"    validate:"required"`
	Payment PaymentMethod `json:"payment" validate:"required"`
}

type UpdatableUser struct {

	Email   string        `json:"email"`
	Plan    Plan          `json:"plan"`
	Payment PaymentMethod `json:"payment"`
}

type LoginUser struct {

	Email   string        `json:"email"`
	Pass    string        `json:"pass"    validate:"required,max=12,min=8,alpha"`

}