package models

type PaymentMethod struct {

	Name  string
	Type  string
	Valid bool
}