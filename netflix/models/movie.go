package models

type Movie struct {
	Title    string `json:"title"`
	Category string `json:"category"`
	Director string `json:"director"`
}
