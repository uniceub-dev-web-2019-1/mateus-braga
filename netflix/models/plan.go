package models


type Plan struct {

	Name         string `validate:"netflix-plan"`
	Value        float32
	ProfileLimit int    `json:"plimit"`
	MaxGraphics  string `json:"maxres"`
	MaxScreens   int    `json:"maxscr"`    
}