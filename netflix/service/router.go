package service


import (
	"github.com/bragamateus/class-codes/class-codes-netflix/netflix/configs"
	"github.com/bragamateus/class-codes/class-codes-netflix/netflix/control"
	"net/http"

		 "github.com/gorilla/mux" )

func createHandler() (handler *mux.Router) {

	// creats router
	handler = mux.NewRouter()

	// associate register user route
	handler.HandleFunc(configs.USER_PATH, control.RegisterUser).Methods(http.MethodPost)
	handler.HandleFunc(configs.MOVIE_PATH, control.RegisterMovie).Methods(http.MethodPost)
	handler.HandleFunc(configs.MOVIE_PATH, control.SearchMovieByCategory).Methods(http.MethodGet)

	// login user
	handler.HandleFunc(configs.LOGIN_PATH, control.LoginUser).Methods(http.MethodPost)

	handler.HandleFunc(configs.USER_PATH, control.UpdateUser).Methods(http.MethodPut)

	// returns handle
	return
}