package main

import (
	"github.com/bragamateus/class-codes/class-codes-netflix/netflix/database"
	"github.com/bragamateus/class-codes/class-codes-netflix/netflix/service"
)

func main() {

	// assure server closing at end of execution
	defer service.StopServer()

	// call db client constructor
	database.CreateClient()

	// call start server function
	service.StartServer()
}