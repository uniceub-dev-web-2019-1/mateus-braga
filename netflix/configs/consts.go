package configs

//=========================
//	Server HTTP 
//=========================
const (

	// defines ip and port address for server instance
	SERVER_ADDR = "localhost:8080"

	// host for mongo db
	MONGO_HOST = "mongodb://localhost:27017"
)

//=========================
//	Paths HTTP 
//=========================
const (

	USER_PATH  = "/user/"
	LOGIN_PATH = "/login/"
	MOVIE_PATH = "/movie/"
)