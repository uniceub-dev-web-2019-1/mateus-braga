package database

import (
	"github.com/bragamateus/class-codes/class-codes-netflix/netflix/models"
	"go.mongodb.org/mongo-driver/bson"
         "context"
         "log" )

func InsertUser(u models.User) (err error) {

	// select users collection
	c := Db.Collection("users")

	// insert user into users collection
	res, err := c.InsertOne(context.TODO(), u)
	
	// checks if any error occurs insert an user
	if err != nil {
		log.Printf("[ERROR] probleming inserting user: %v %v", err, res.InsertedID)
		return
	}

	return
}

func SearchUser(l models.LoginUser) (u models.User, err error) {

	// select users collection
	c := Db.Collection("users")

	// create filter
	filter := bson.D{{"email", l.Email}, {"pass", l.Pass}}

	// try to find user with login
	err = c.FindOne(context.TODO(), filter).Decode(&u)

	// checks if any error occurs insert an user
	if err != nil {
		log.Printf("[ERROR] probleming searching user: %v %v", err, u)
		return
	}

	log.Printf("[INFO] searching user: %v %v", err, u)

	return
}