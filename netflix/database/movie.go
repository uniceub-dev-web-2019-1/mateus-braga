package database

import (
	"context"
	"github.com/bragamateus/class-codes/class-codes-netflix/netflix/models"
	"github.com/globalsign/mgo/bson"
	"log"
)

func InsertMovie(m models.Movie) (err error) {

	// select users collection
	c := Db.Collection("movie")

	// insert user into users collection
	res, err := c.InsertOne(context.TODO(), m)

	// checks if any error occurs insert an user
	if err != nil {
		log.Printf("[ERROR] probleming inserting movie: %v %v", err, res)
		return
	}

	return
}

func SearchMovie(s string) (m models.Movie, err error) {

	// select users collection
	c := Db.Collection("movie")

	// create filter
	filter := bson.M{"category": s}

	// try to find user with login
	err = c.FindOne(context.TODO(), filter).Decode(&m)

	// checks if any error occurs insert an movie
	if err != nil {
		log.Printf("[ERROR] probleming searching user: %v %v", err, m)
		return
	}

	log.Printf("[INFO] searching user: %v %v", err, m)

	return
}
