package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
)

type Numbers struct {
	N1 float64  `json:"num_1"`
	N2 float64	`json:"num_2"`
}

type SomaResultado struct {
	Result float64 `json:"result"`
}

func CalcHandler(w http.ResponseWriter, r *http.Request) {

	soma := Numbers{}

	body := r.Body

	err := DecodeJson(body, &soma)

	resultado := SomaResultado{Result: soma.adicao()}
	jsonResultado, _ := json.Marshal(resultado)

	if err != nil{
		w.Write([]byte("400"))
	}

	w.Write([]byte(jsonResultado))


}

func (soma *Numbers) adicao() float64{
	return soma.N1 + soma.N2
}

func DecodeJson(payload io.Reader, entity interface{}) (err error){

	decoder := json.NewDecoder(payload)

	err = decoder.Decode(entity)

	if err != nil {
		log.Printf("Não foi possível converter: %v", err)
		return
	}

	return
}


