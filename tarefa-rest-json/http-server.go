package main

import (
	"log"
	"net/http"
	"time"
)

func StartServer() {

	// declaração e atribuição do servidor http
	server := http.Server{Addr: "localhost:8080",
		Handler: createRouter(),
		ReadTimeout:  100 * time.Millisecond,
		WriteTimeout: 200 * time.Millisecond,
		IdleTimeout:  50 * time.Millisecond}

	// inicializar o servidor
	log.Print(server.ListenAndServe())
}

