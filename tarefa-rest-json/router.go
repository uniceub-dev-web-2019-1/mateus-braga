package main

import "github.com/gorilla/mux"

func createRouter() (r *mux.Router) {

	r = mux.NewRouter()

	r.HandleFunc("/calculator/soma", CalcHandler).Methods("POST").Headers("Content-Type", "application/json")

	return
}