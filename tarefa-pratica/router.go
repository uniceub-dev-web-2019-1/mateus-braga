package main

import ( "github.com/gorilla/mux" )

func createRouter() (r *mux.Router) {

	r = mux.NewRouter()

	r.HandleFunc("/user", UserHandler).Methods("GET").Queries("name", "{name}")
	r.HandleFunc("/user", UserHandler).Methods("POST").Headers("Content-Type")
	r.HandleFunc("/user", UserHandler).Methods("PUT")

	return
}
